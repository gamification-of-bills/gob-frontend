import 'package:flutter_test/flutter_test.dart';
import 'package:frontend/Cache/MemCache.dart';

void main() {
  group('MemCache tests', () {
    test('Caching a request saves for future returning', () async {
      final memCache = MemCache();

      var obj = "test";

      memCache.put(obj.hashCode, obj);

      expect(obj, await memCache.get(obj.hashCode));
    });
  });
}
