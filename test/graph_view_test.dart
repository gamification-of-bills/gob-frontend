import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter_test/flutter_test.dart';
import 'package:frontend/Components/GraphView.dart';
import 'package:frontend/Models/Usage.dart';
import 'package:intl/intl.dart';

void main() {
  GraphView graph = GraphView.withSampleData();

  var listOne = [Usage(startTime: DateTime.now(), consumption: 1)];
  var listTwo = [Usage(startTime: DateTime.now(), consumption: 1)];

  group('Graph data tests', () {
    test('GraphView has List of Usage to show', () {
      expect(
          graph.series, isInstanceOf<List<charts.Series<Usage, DateTime>>>());
    });

    test('GraphView with single list contains single list', () {
      GraphView singleGraph = GraphView.createGraphView(listOne, "test", DateFormat.yMMMMd());

      expect(singleGraph.series.length, 1);
    });

    test('GraphView with two lists contains two lists', () {
      GraphView dualGraph =
          GraphView.createGraphView(listOne, "test", DateFormat.yMMMMd(), compareUsageList: listTwo);

      expect(dualGraph.series.length, 2);
    });

    test('Passing highlightPeaks creates highlightRange', () {
      GraphView graphView =
      GraphView.createGraphView(listOne, "test", DateFormat.yMMMMd(), highlightPeaks: true,);

      expect(graphView.highlightRange.annotations, isNot([]));
    });

    test('Not passing highlightPeaks doesnt create highlightRange', () {
      GraphView graphView =
      GraphView.createGraphView(listOne, "test", DateFormat.yMMMMd());

      expect(graphView.highlightRange.annotations, []);
    });

    test('Graphview has uses custom properties', () {
      GraphView graphView =
      GraphView.createGraphView(listOne, "test", DateFormat.yMMMMd());

      expect(graphView.axisSpec.tickProviderSpec, isInstanceOf<charts.AutoDateTimeTickProviderSpec>());
    });
  });
}
