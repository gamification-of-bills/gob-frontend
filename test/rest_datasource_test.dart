import 'dart:convert';
import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:frontend/Data/RestDatasource.dart';
import 'package:frontend/Models/IntervalResponse.dart';
import 'package:frontend/Models/LoginResponse.dart';
import 'package:frontend/Models/StatsResponse.dart';
import 'package:frontend/app_config.dart';
import 'package:http/http.dart' as http;
import 'package:mockito/mockito.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MockClient extends Mock implements http.Client {}

void main() {
  group('fetchData', () {
    final client = MockClient();
    final restDs = RestDatasource();
    SharedPreferences.setMockInitialValues({"flutter.accessToken": "test"});

    test('returns a IntervalResponse if the http call completes successfully',
        () async {
      when(client.post(defaultConfig.apiUrl + '/api/interval', headers: {
        HttpHeaders.authorizationHeader: "Bearer test"
      }, body: {
        'resolution': '2',
        'startDate': '2018-12-01T00:00:00.000',
        'endDate': '2019-01-01T00:00:00.000'
      })).thenAnswer((_) async => http.Response(
          '{"id":0,"startDate":"2018-12-01T00:00:00","endDate":"2019-01-01T00:00:00","dataPoints":[]}',
          200));

      expect(
          await restDs.fetchIntervalData(
              client,
              2,
              DateTime.parse("2018-12-01T00:00:00"),
              DateTime.parse("2019-01-01T00:00:00")),
          isInstanceOf<IntervalResponse>());
    });

    test('throws an exception if the http call completes with an error', () {
      when(client.post(defaultConfig.apiUrl + '/api/interval', headers: {
        HttpHeaders.authorizationHeader: "Bearer test"
      }, body: {
        'resolution': '2',
        'startDate': '2018-12-02T00:00:00.000',
        'endDate': '2019-01-01T00:00:00.000'
      })).thenAnswer((_) async => http.Response("Error", 404));

      expect(
          restDs.fetchIntervalData(
              client,
              2,
              DateTime.parse("2018-12-02T00:00:00"),
              DateTime.parse("2019-01-01T00:00:00")),
          throwsException);
    });
  });

  group('login', () {
    final client = MockClient();
    final restDs = RestDatasource();
    test('login works with valid creds', () async {
      when(
          client.post(defaultConfig.apiUrl + '/auth/signin',
              headers: {"Content-type": 'application/json'},
              body: json.encode({
                'username': 'test',
                'password': 'password'
              }))).thenAnswer((_) async => http.Response(
          '{"accessToken":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ==.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c","tokenType":"Bearer"}',
          200));

      expect(await restDs.login(client, "test", "password"),
          isInstanceOf<LoginResponse>());
    });

    test('throws exception with invalid creds', () {
      when(client.post(defaultConfig.apiUrl + '/auth/signin',
              headers: {"Content-type": 'application/json'},
              body: json.encode({'username': 'test', 'password': 'invalid'})))
          .thenAnswer((_) async => http.Response('Forbidden', 403));

      expect(restDs.login(client, "test", "invalid"), throwsException);
    });
  });

  group('stats', () {
    final client = MockClient();
    final restDs = RestDatasource();
    SharedPreferences.setMockInitialValues({"flutter.accessToken": "test"});

    test('returns a StatsResponse if the http call completes successfully',
            () async {
          when(client.post(defaultConfig.apiUrl + '/api/stats', headers: {
            HttpHeaders.authorizationHeader: "Bearer test"
          }, body: {
            'startDate': '2019-06-01T00:00:00.000'
          })).thenAnswer((_) async => http.Response(
              '{"startDate":"2019-06-01T00:00:00","min":0.0,"max":2256.831,"average":202.41676907630523}',
              200));

          expect(
              await restDs.fetchStats(
                  client,
                  DateTime.parse("2019-06-01T00:00:00")),
              isInstanceOf<StatsResponse>());
        });

    test('throws an exception if the http call completes with an error', () {
      when(client.post(defaultConfig.apiUrl + '/api/stats', headers: {
        HttpHeaders.authorizationHeader: "Bearer test"
      }, body: {
        'startDate': '2019-06-01T00:00:00.000'
      })).thenAnswer((_) async => http.Response("Error", 404));

      expect(
          restDs.fetchStats(
              client,
              DateTime.parse("2019-06-01T00:00:00")),
          throwsException);
    });
  });

}
