// Imports the Flutter Driver API
import 'dart:io';

import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

takeScreenshot(FlutterDriver driver, String path) async {
  final List<int> pixels = await driver.screenshot();
  final File file = new File(path);
  await file.writeAsBytes(pixels);
  print(path);
}

void main() {
  group('Frontend app', () {
    final loginTextFinder = find.byValueKey('Username');
    final passwordTextFinder = find.byValueKey('Password');

    final loginButtonFinder = find.byValueKey('Login');

    FlutterDriver driver;

    // Connect to the Flutter driver before running any tests
    setUpAll(() async {
      driver = await FlutterDriver.connect();
      new Directory('screenshots').create();
    });

    // Close the connection to the driver after the tests have completed
    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });

    test('check flutter driver health', () async {
      Health health = await driver.checkHealth();
      print(health.status);
    });

    test('login works', () async {
      // Use the `driver.getText` method to verify the counter starts at 0.
      await driver.tap(loginTextFinder);
      await driver.enterText("1");
      await driver.tap(passwordTextFinder);
      await driver.enterText("prince");

      await driver.tap(loginButtonFinder);

      await driver.waitFor(find.text("Day view"));
    });
  });
}