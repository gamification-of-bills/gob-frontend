import 'package:flutter/material.dart';
import 'package:frontend/Components/GraphView.dart';
import 'package:frontend/Components/MonthView.dart';
import 'package:frontend/Components/UniversalScaffold.dart';
import 'package:frontend/Data/RestDatasource.dart';
import 'package:frontend/Data/comparePop.dart';
import 'package:frontend/Models/IntervalResponse.dart';
import 'package:frontend/Models/Usage.dart';
import 'package:frontend/Screens/Month/index.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'package:preferences/preference_service.dart';

enum PopupMenu { compare }

class DayViewScreen extends StatefulWidget {
  const DayViewScreen({Key key}) : super(key: key);

  @override
  DayViewScreenState createState() => DayViewScreenState();
}

class DayViewScreenState extends State<DayViewScreen> {
  DateTime currentDate = DateTime.now();
  DateTime comparisonDate;

  int resolution = 2;

  double conversion;
  String units;

  final Map<String, double> conversionRates = {
    'kWh': 1,
    '£/H': 0.12978,
    'Nissan Leaf': 50 / 17,
    '2 Litre Kettles': 40 / 9,
  };

  final client = http.Client();
  final restDs = RestDatasource();

  Future<IntervalResponse> intervalResponse;
  Future<IntervalResponse> comparisonResponse;

  List<Widget> actions;

  DateTime _increment(DateTime date) {
    return DateTime(date.year, date.month, date.day + 1);
  }

  DateTime _decrement(DateTime date) {
    return DateTime(date.year, date.month, date.day - 1);
  }

  DateTime _extractRelevant(DateTime date) {
    return DateTime(date.year, date.month, date.day);
  }

  void _selectForward() {
    setState(() {
      currentDate = _increment(currentDate);
      intervalResponse = restDs.fetchIntervalData(
          client, resolution, currentDate, _increment(currentDate));
    });
  }

  void _selectBackward() {
    setState(() {
      currentDate = _decrement(currentDate);
      intervalResponse = restDs.fetchIntervalData(
          client, resolution, currentDate, _increment(currentDate));
    });
  }

  Future<IntervalResponse> _fetchData(http.Client client,
      [DateTime startDate, DateTime endDate]) {
    return restDs.fetchIntervalData(client, resolution, startDate, endDate);
  }

  void _selectDate() {
    showDatePicker(
            context: context,
            initialDate: currentDate,
            firstDate: DateTime.utc(0),
            lastDate: DateTime.utc(100000))
        .then((DateTime date) {
      if (date != null)
        setState(() {
          currentDate = _extractRelevant(date);
          intervalResponse = restDs.fetchIntervalData(
              client, resolution, currentDate, _increment(currentDate));
        });
    });
  }

  void updateUnitsCallback(String u) {
    setState(() {
      units = u ?? '£/H';
      conversion = conversionRates[u];
    });
  }

  @override
  void initState() {
    super.initState();

    units = PrefService.getString('default_unit') ?? '£/H';
    conversion = conversionRates[units];

    resolution = 0;
    currentDate = _extractRelevant(currentDate);
    intervalResponse = _fetchData(client, currentDate, _increment(currentDate));
    if (comparisonDate != null)
      comparisonResponse =
          _fetchData(client, comparisonDate, _increment(comparisonDate));
    else
      comparisonResponse = _fetchData(client);

    actions = <Widget>[
      ComparePopup(updateUnitsCallback),
      PopupMenuButton<PopupMenu>(
        onSelected: (PopupMenu result) {
          if (result == PopupMenu.compare) {
            showDatePicker(
                    context: context,
                    initialDate: currentDate,
                    firstDate: DateTime.utc(0),
                    lastDate: DateTime.utc(100000))
                .then((DateTime date) {
              if (date != null)
                setState(() {
                  comparisonDate = _extractRelevant(date);
                  comparisonResponse = _fetchData(
                      client, comparisonDate, _increment(comparisonDate));
                });
            });
          }
        },
        itemBuilder: (BuildContext context) => <PopupMenuEntry<PopupMenu>>[
              const PopupMenuItem<PopupMenu>(
                value: PopupMenu.compare,
                child: Text('Compare to another day'),
              ),
            ],
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return UniversalScaffold(
        appBarParams: AppBarParams(title: Text('Day view'), actions: actions),
        route: '/day',
        body: Column(
          children: <Widget>[
            MonthView(
              month: DateFormat.yMMMMd().format(currentDate),
              selectBackward: _selectBackward,
              selectForward: _selectForward,
              selectDate: _selectDate,
            ),
            Flexible(
              child: FutureBuilder(
                future:
                    Future.wait([intervalResponse, comparisonResponse]).then(
                  (response) => MergedResponses(response[0], response[1]),
                ),
                builder: (context, AsyncSnapshot<MergedResponses> snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.none:
                    case ConnectionState.active:
                    case ConnectionState.waiting:
                      return CircularProgressIndicator();
                    case ConnectionState.done:
                      if (snapshot.hasError)
                        return Text("${snapshot.error}, check connection");
                  }
                  if (snapshot.data.current.dataPoints.length != 0) {
                    var currentDp = snapshot.data.current.dataPoints
                        .map((Usage u) => Usage(
                            startTime: u.startTime,
                            consumption: u.consumption * conversion))
                        .toList();
                    if (snapshot.data.comparison != null &&
                        snapshot.data.comparison.dataPoints.length != 0) {
                      var compareDp = snapshot.data.comparison.dataPoints
                          .map((Usage u) => Usage(
                              startTime: u.startTime,
                              consumption: u.consumption * conversion))
                          .toList();
                      return GraphView.createGraphView(
                          currentDp, units, DateFormat.yMMMMd(),
                          compareUsageList: compareDp, highlightPeaks: true,);
                    } else
                      return GraphView.createGraphView(
                          currentDp, units, DateFormat.yMMMMd(), highlightPeaks: true);
                  } else
                    return Text("No data");
                },
              ),
            )
          ],
        ));
  }
}
