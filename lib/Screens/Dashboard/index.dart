import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:frontend/Components/UniversalScaffold.dart';
import 'package:frontend/Data/RestDatasource.dart';
import 'package:frontend/Models/IntervalResponse.dart';
import 'package:frontend/Models/StatsResponse.dart';
import 'package:frontend/Models/Usage.dart';
import 'package:http/http.dart' as http;

class DashboardScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => DashboardScreenState();
}

class DonutPieChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  DonutPieChart(this.seriesList, {this.animate});

  factory DonutPieChart.withRealData(double usage, double total) {
    List<PieChartSegment> data = [];

    if (usage > total)
      data.add(PieChartSegment(0, total, usage.toStringAsFixed(2), 0xFFC3424C));
    else {
      data.add(PieChartSegment(0, usage, usage.toStringAsFixed(2), 0xFFC3424C));
      data.add(PieChartSegment(1, total - usage, total.toStringAsFixed(2), 0xFF9C27B0));
    }

    return DonutPieChart(
      [
        charts.Series<PieChartSegment, int>(
          id: 'progress',
          domainFn: (PieChartSegment segment, _) => segment.id,
          measureFn: (PieChartSegment segment, _) => segment.value,
          colorFn: (PieChartSegment segment, __) => charts.ColorUtil.fromDartColor(Color(segment.color)),
          //Data for text on chart
          labelAccessorFn: (PieChartSegment row, _) => '${row.label}',
          data: data,
        )
      ],
      // Disable animations for image tests.
      animate: false,
    );
  }

  @override
  Widget build(BuildContext context) {
    return charts.PieChart(seriesList,
        animate: animate,
        // Configure the width of the pie slices to 60px. The remaining space in
        // the chart will be left as a hole in the center.
        defaultRenderer:
            charts.ArcRendererConfig(arcWidth: 50, startAngle: -3 / 2 * pi,
                //Renders text on chart
                arcRendererDecorators: [charts.ArcLabelDecorator()]));
  }
}

class PieChartSegment {
  final int id;
  final double value;
  final String label;
  final int color;

  PieChartSegment(this.id, this.value, this.label, this.color);
}

class DashboardScreenState extends State<DashboardScreen> {
  List<Widget> actions;
  DateTime date = DateTime.now();

  final client = http.Client();
  final restDs = RestDatasource();

  Future<IntervalResponse> monthData;
  Future<double> monthUsage;

  Future<StatsResponse> stats;

  @override
  void initState() {
    super.initState();
    monthData = restDs.fetchIntervalData(
        client, 2, DateTime(date.year, date.month), date);
    monthUsage = monthData.then((IntervalResponse v) {
      return v.dataPoints.fold<double>(0, (a, Usage b) => a + b.consumption);
    });

    stats = restDs.fetchStats(client, DateTime(date.year, date.month-1));
  }

  Widget goals() {
    List<String> goalList = [
      "Consume 5% less energy compared to last month",
      "Use up 10% less energy during peak times compared to the previous month"
    ];
    var randomItem = (goalList..shuffle()).first;
    return ListTile(
        title: Text("Goal", textAlign: TextAlign.center),
        subtitle: Text(randomItem));
  }

  List<String> tipList = [
    "Not sure what something does? Hold your finger over it and an explanation will pop-up",
    "Kilo-what-hours? Convert kWh to other units to make your usage views easier to interpret",
    "Using less energy during peak times will both help the environment and your bills!"
//    "Not only does a dark theme strain your eyes less in prolonged screen sessions, it also helps with battery, and thus energy usage! You can enable it in the settings"
  ];

  Widget tips() {
    var randomItem = (tipList..shuffle()).first;
    return ListTile(
      title: Text(
        "Tip",
        textAlign: TextAlign.center,
      ),
      subtitle: Text(randomItem),
    );
  }

  @override
  Widget build(BuildContext context) {
    return UniversalScaffold(
      appBarParams: AppBarParams(title: Text('Dashboard'), actions: actions),
      route: '/dash',
      body: Container(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Card(
                child: ListTile(
                  title: Text(
                    "Overview",
                    textAlign: TextAlign.center,
                  ),
                  subtitle: FutureBuilder(
                    future: monthUsage,
                    builder: (context, snapshot) {
                      switch (snapshot.connectionState) {
                        case ConnectionState.none:
                        case ConnectionState.active:
                        case ConnectionState.waiting:
                          return CircularProgressIndicator();
                        case ConnectionState.done:
                          if (snapshot.hasError)
                            return Text("${snapshot.error}, check connection");
                      }
                      return Text(
                          "You have consumed the equivalent of ${snapshot.data.toStringAsFixed(2)} units so far this month");
                    },
                  ),
                ),
              ),
              Card(
                  child: Column(
                children: <Widget>[
                  goals()
                ],
              )),
              SizedBox(
                width: double.infinity,
                child: Card(
                  child: ListTile(
                    title: Text("Current month", textAlign: TextAlign.center,),
                    subtitle: FutureBuilder(
                        future: Future.wait([monthUsage, stats]),
                        builder: (context, snapshot) {
                          if (snapshot.connectionState != ConnectionState.done)
                            return Column(
                              children: <Widget>[
                                CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(Colors.pink)),
                              ],
                            );
                          if (snapshot.hasError)
                            return Text("${snapshot.error}, check connection");
                          double used = snapshot.data[0];
                          StatsResponse stats = snapshot.data[1];
                          return Column(
                            children: <Widget>[
                              Text("This month you've used ${used.toStringAsFixed(2)}kWh compared to the average ${stats.average.toStringAsFixed(2)}kWh"),
                              Container(
                                margin: const EdgeInsets.all(20),
                                height: 260,
                                child: Stack(
                                  children: [
                                    DonutPieChart.withRealData(
                                      used, stats.average),
                                    Center(
                                      child: Text("${(used/stats.average * 100).toStringAsFixed(0)}%", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
                                    )
                                ])),
                              used < stats.average ?
                                  Text("Below average! Well done, keep it up!") :
                                  Text("You're above average usage this month, watch your usage carefully next month to improve!")
                          ]);
                        }),
                  ),
                ),
              ),
              Card(child: tips())
            ],
          )),
    );
  }
}
