import 'package:flutter/material.dart';
import 'dart:async';

class StaggerAnimationPartOne extends StatelessWidget {
  StaggerAnimationPartOne({Key key, this.buttonController})
      : buttonSqueezeAnimation = Tween(
          begin: 320.0,
          end: 70.0,
        ).animate(
          CurvedAnimation(
            parent: buttonController,
            curve: Interval(
              0.0,
              0.150,
            ),
          ),
        ),
        super(key: key);

  final AnimationController buttonController;
  final Animation buttonSqueezeAnimation;

  Future<Null> _playAnimation() async {
    try {
      await buttonController.forward();
    } on TickerCanceled {}
  }

  Widget _buildAnimation(BuildContext context, Widget child) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 50.0),
      child: InkWell(
          onTap: () {
            _playAnimation();
          },
          child: Hero(
              tag: "fade",
              child: Container(
                  width: buttonSqueezeAnimation.value,
                  height: 60.0,
                  alignment: FractionalOffset.center,
                  decoration: BoxDecoration(
                    color: const Color(0xFFF7406A),
                    borderRadius: BorderRadius.all(const Radius.circular(30.0)),
                  ),
                  child: buttonSqueezeAnimation.value > 75.0
                      ? Text(
                          "Sign In",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20.0,
                            fontWeight: FontWeight.w300,
                            letterSpacing: 0.3,
                          ),
                        )
                      : CircularProgressIndicator(
                          value: null,
                          strokeWidth: 1.0,
                          valueColor:
                              AlwaysStoppedAnimation<Color>(Colors.white),
                        )))),
    );
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      builder: _buildAnimation,
      animation: buttonController,
    );
  }
}

class StaggerAnimationPartTwo extends StatelessWidget {
  StaggerAnimationPartTwo({Key key, this.buttonController})
      : buttonZoomOut = Tween(
          begin: 0.0,
          end: 1.0,
        ).animate(
          CurvedAnimation(
            parent: buttonController,
            curve: Interval(
              0.1,
              0.999,
              curve: Curves.ease,
            ),
          ),
        ),
        containerCircleAnimation = EdgeInsetsTween(
          begin: const EdgeInsets.only(bottom: 50.0),
          end: const EdgeInsets.only(bottom: 0.0),
        ).animate(
          CurvedAnimation(
            parent: buttonController,
            curve: Interval(
              0.0,
              0.9,
              curve: Curves.ease,
            ),
          ),
        ),
        super(key: key);

  final AnimationController buttonController;
  final Animation<EdgeInsets> containerCircleAnimation;
  final Animation buttonZoomOut;

  Future<Null> _playAnimation() async {
    try {
      await buttonController.forward();
    } on TickerCanceled {}
  }

  Widget _buildAnimation(BuildContext context, Widget child) {
    var height = MediaQuery.of(context).size.height;
    return Padding(
      padding: buttonZoomOut.value == 0.0
          ? const EdgeInsets.only(bottom: 50.0)
          : containerCircleAnimation.value,
      child: InkWell(
          child: Hero(
        tag: "fade",
        child: buttonZoomOut.value <= 300
            ? Container(
                width: buttonZoomOut.value == 0.0
                    ? 70.0
                    : height * buttonZoomOut.value,
                height: buttonZoomOut.value == 0.0
                    ? 60.0
                    : height * buttonZoomOut.value,
                alignment: FractionalOffset.center,
                decoration: BoxDecoration(
                  color: const Color(0xFFF7406A),
                  borderRadius: buttonZoomOut.value * height < 400
                      ? BorderRadius.all(const Radius.circular(30.0))
                      : BorderRadius.all(const Radius.circular(0.0)),
                ),
                child: null)
            : Container(
                width: buttonZoomOut.value * height,
                height: buttonZoomOut.value * height,
                decoration: BoxDecoration(
                  shape: buttonZoomOut.value * height <
                          MediaQuery.of(context).size.width
                      ? BoxShape.circle
                      : BoxShape.rectangle,
                  color: const Color(0xFFF7406A),
                ),
              ),
      )),
    );
  }

  @override
  Widget build(BuildContext context) {
    buttonController.addListener(() {
      if (buttonController.isCompleted) {
        Navigator.pushReplacementNamed(context, "/day");
      }
    });
    return AnimatedBuilder(
      builder: _buildAnimation,
      animation: buttonController,
    );
  }
}
