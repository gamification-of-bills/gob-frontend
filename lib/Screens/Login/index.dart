import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:frontend/Components/Forms.dart';
import 'package:frontend/Components/Logo.dart';
import 'package:frontend/Components/SignInButton.dart';
import 'package:frontend/Data/RestDatasource.dart';
import 'package:frontend/Screens/Login/loginAnimation.dart';
import 'package:frontend/Screens/Login/styles.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key key}) : super(key: key);

  @override
  State createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen>
    with TickerProviderStateMixin {
  final _usernameInputController = TextEditingController();
  final _passwordInputController = TextEditingController();
  final client = http.Client();

  AnimationController _loginButtonController;
  AnimationController _nextPageController;
  var animationStatus = 0;

  @override
  void initState() {
    super.initState();
    _loginButtonController = AnimationController(
        duration: Duration(milliseconds: 3000), vsync: this);
    _nextPageController =
        AnimationController(duration: Duration(milliseconds: 500), vsync: this);
  }

  @override
  void dispose() {
    _loginButtonController.dispose();
    _nextPageController.dispose();
    super.dispose();
  }

  Future<Null> _playFirstAnimation() async {
    try {
      await _loginButtonController.forward();
    } on TickerCanceled {}
  }

  Future<Null> _playSecondAnimation() async {
    try {
      await _nextPageController.forward();
    } on TickerCanceled {}
  }

  Future<bool> _loginRequest(context, _username, _password) async {
    final client = http.Client();
    var restDs = RestDatasource();
    try {
      var loginRes = await restDs.login(client, _username, _password);
      SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.setString('username', _username);
      await prefs.setString('accessToken', loginRes.accessToken);
      await prefs.setInt('exp', loginRes.exp);
      return true;
    } catch (e) {
      print(e);
      showDialog(
          context: context,
          builder: (_) => AlertDialog(
                content: Text(
                  "Failed to login",
                  style: TextStyle(color: Colors.black),
                ),
              ));
      setState(() {
        animationStatus = 0;
        _loginButtonController.reverse();
      });
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        backgroundColor: Colors.white,
        body: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment(1, 0),
                    colors: [Color(0xFF274B63), Color(0xFF6C5CC0)],
                    tileMode: TileMode.repeated)),
            child: Stack(
                alignment: AlignmentDirectional.bottomCenter,
                children: <Widget>[
                  Column(
//                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Container(
                          padding: const EdgeInsets.only(top: 150, bottom: 50),
                          child: Center(child: Logo(image: logo)),
                        ),
                        FormContainer(
                            usernameController: _usernameInputController,
                            passwordController: _passwordInputController),
                      ]),
                  animationStatus == 0
                      ? Padding(
                          padding: const EdgeInsets.only(bottom: 50.0),
                          child: InkWell(
                            key: Key("Login"),
                              onTap: () {
                                setState(() {
                                  animationStatus = 1;
                                });
                                _playFirstAnimation();
                              },
                              child: SignIn()),
                        )
                      : FutureBuilder(
                          future: _loginRequest(
                              context,
                              _usernameInputController.text,
                              _passwordInputController.text),
                          builder: (BuildContext context,
                              AsyncSnapshot<bool> snapshot) {
                            switch (snapshot.connectionState) {
                              case ConnectionState.done:
                                _playSecondAnimation();
                                return StaggerAnimationPartTwo(
                                  buttonController: _nextPageController.view,
                                );
                              default:
                                return StaggerAnimationPartOne(
                                    buttonController:
                                        _loginButtonController.view);
                            }
                          },
                        )
                ])));
  }
}
