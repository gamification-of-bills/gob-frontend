import 'package:flutter/material.dart';

class MonthView extends StatelessWidget {
  final VoidCallback selectBackward;
  final VoidCallback selectForward;
  final VoidCallback selectDate;
  final String month;

  MonthView({this.selectBackward, this.selectForward, this.selectDate, this.month});

  @override
  Widget build(BuildContext context) {
    return (Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: selectBackward,
        ),
        InkWell(
          onTap: selectDate,
          child: Text(month.toUpperCase(),
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.title
//            style: new TextStyle(
//                fontSize: 18.0, letterSpacing: 1.2, fontWeight: FontWeight.w300),
              ),
        ),
        IconButton(
          icon: Icon(Icons.arrow_forward),
          onPressed: selectForward,
        ),
      ],
    ));
  }
}
