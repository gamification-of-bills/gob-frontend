import 'package:flutter/material.dart';
import 'package:frontend/Screens/Day/index.dart';
import 'package:frontend/Screens/Login/index.dart';
import 'package:frontend/Screens/Dashboard/index.dart';
import 'package:frontend/Screens/Month/index.dart';
import 'package:frontend/Screens/Settings/index.dart';
import 'package:frontend/Screens/Year/index.dart';
import 'package:frontend/app_config.dart';

class Routes {
  Routes() {
    runApp(MaterialApp(
      title: defaultConfig.appName,
      theme: defaultConfig.theme,
      home: LoginScreen(),
      onGenerateRoute: (RouteSettings settings) {
        switch (settings.name) {
          case '/login':
            return MyCustomRoute(
              builder: (_) => LoginScreen(),
              settings: settings,
            );
          case '/dash':
            return MyCustomRoute(
              builder: (_) => DashboardScreen(),
              settings: settings,
            );

          case '/month':
            return MyCustomRoute(
              builder: (_) => MonthViewScreen(),
              settings: settings,
            );

          case '/day':
            return MyCustomRoute(
              builder: (_) => DayViewScreen(),
              settings: settings,
            );

          case '/year':
            return MyCustomRoute(
              builder: (_) => YearViewScreen(),
              settings: settings,
            );

          case '/settings':
            return MyCustomRoute(
              builder: (_) => SettingsScreen(),
              settings: settings,
            );
        }
      },
    ));
  }
}

class MyCustomRoute<T> extends MaterialPageRoute<T> {
  MyCustomRoute({WidgetBuilder builder, RouteSettings settings})
      : super(builder: builder, settings: settings);

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    if (settings.isInitialRoute) return child;
    return FadeTransition(opacity: animation, child: child);
  }
}
