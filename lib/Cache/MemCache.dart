import 'dart:collection';

import 'package:frontend/Cache/Cache.dart';

class MemCache<T> extends Cache<T> {
  final map = HashMap<int, T>();

  @override
  Future<T> get(int index) {
    return Future.value(map[index]);
  }

  @override
  put(int index, T object) {
    map[index] = object;
  }
}
